@extends('admin.layouts.app')

@section('after_style')
    <link href="https://cdn.datatables.net/v/dt/dt-1.10.18/sc-1.5.0/datatables.min.css" rel="stylesheet" type="text/css" />
@endsection

@section('section_title')
    Gov Requests
@endsection

@section('content')

    @if($flash = session('message'))
        <div class="alert alert-success">
            <i class="fa fa-bell" aria-hidden="true"></i>
            {{ $flash }}
        </div>
    @endif

    <table id="table" class="table table-striped table-hover table-bordered">
        <thead class="thead-dark">
        <tr>
            <th>Request Number</th>
            <th>User</th>
            <th>Title</th>
            <th>Category</th>
            <th>Ministry</th>
            <th style="width: 150px;">Created_at</th>
            <th>Action</th>
        </tr>
        </thead>
    </table>

    <div class="row" style="margin-top: 20px;">
        <div class="col-md-12 text-right">
            <a href="{{ route('create_gov_request') }}" class="btn btn-primary">Add New Request</a>
        </div>
    </div>

@endsection

@section('after_script')

    <script src="https://cdn.datatables.net/v/dt/dt-1.10.18/sc-1.5.0/datatables.min.js"></script>
    <script>
        $(document).ready(function() {
            $('#table').DataTable({
                order: [0,'desc'],
                processing: true,
                serverSide: true,
                ajax: "{{ url('/api/admin/gov_requests_list') }}",
                columns: [
                    { "data": "request_unique_number" },
                    { "data": "user" },
                    { "data": "title" },
                    { "data": "category" },
                    { "data": "ministry" },
                    {
                        "data": "created_at",
                        "searchable": false,
                        "orderable":false
                    },
                    { "data": "action" }
                ]
            });

        });
    </script>

@endsection
