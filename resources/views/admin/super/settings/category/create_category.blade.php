@extends('admin.layouts.app')

@section('after_style')
    <link href="https://cdn.datatables.net/v/dt/dt-1.10.18/sc-1.5.0/datatables.min.css" rel="stylesheet" type="text/css" />
@endsection

@section('section_title')
    Add New Category
@endsection

@section('content')

    @include('errors.errors')
    @if($flash = session('message'))
        <div class="alert alert-success">
            <i class="fa fa-bell" aria-hidden="true"></i>
            {{ $flash }}
        </div>
    @endif
    <div class="row">
        <div class="col-md-8">
            <form action="{{ url('admin/settings/category') }}" method="post">
                {{ csrf_field() }}
                <div class="form-group row">
                    <label class="col-md-4 col-form-label text-right">Name Ar</label>
                    <div class="col-md-6">
                        <input type="text" name="name_ar" class="form-control" required autofocus>
                    </div>
                </div>
                <div class="form-group row">
                    <label class="col-md-4 col-form-label text-right">Name FR</label>
                    <div class="col-md-6">
                        <input type="text" name="name_fr" class="form-control" required autofocus>
                    </div>
                </div>
                <div class="form-group row">
                    <label class="col-md-4 col-form-label text-right">Name EN</label>
                    <div class="col-md-6">
                        <input type="text" name="name_en" class="form-control" required autofocus>
                    </div>
                </div>
                <div class="form-group row">
                    <label class="col-md-4 col-form-label text-right">Active</label>
                    <div class="col-md-6">
                        <select name="active" class="form-control" required>
                            <option value="1">Yes</option>
                            <option value="0">No</option>
                        </select>
                    </div>
                </div>
                <div class="col-md-8 offset-md-4">
                    <button class="btn btn-primary">Add New One</button>
                </div>
            </form>
        </div>
    </div>

@endsection
