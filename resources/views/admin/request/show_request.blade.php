@extends('admin.layouts.app')

@section('after_style')

    <!-- bootstrap datepicker -->
    <link rel="stylesheet" href="{{ asset('bower_components/bootstrap-datepicker/dist/css/bootstrap-datepicker.min.css') }}">

@endsection


@section('section_title')
    REQUEST {{ $request->id }}, {{ $request->title }}
@endsection

@section('content')

    <div class="row">
        @if($flash = session('message'))
            <div class="col-md-12">
                <div class="alert alert-success">
                    <i class="fa fa-bell" aria-hidden="true"></i>
                    {{ $flash }}
                </div>
            </div>
        @endif
        <div class="col-md-12">
            @include('errors.errors')
        </div>

        <div class="col-md-7">
            @if($request->proof_file)
                <div style="margin-top: 15px; margin-bottom: 15px;">
                    <a href="{{ asset($request->proof_file) }}" class="btn btn-primary">Preuve de contact</a>
                </div>
            @endif
            <p style="font-size: 20px;">
                {{ $request->description }}
            </p>
            <h1 style="font-size: 20px;">{{ $request->category->name }}</h1>
            <comment-component-admin
                :app_url="'{{url('/')}}'"
                :user="{{ auth()->user() }}"
                :update="'{{ (int) $update }}'"
                :request="{{ $request }}"
            ></comment-component-admin>
        </div>

        <div class="col-md-5">
            <div class="card shadow">
                <div class="card-body">
                    <ul class="list-group list-group-flush">
                        <li class="list-group-item">
                            Créé par <a href="{{ route('show_applicant', [$request->applicant->id]) }}">{{ $request->applicant->name }}</a>, le {{ $request->created_at }}
                        </li>
                        <li class="list-group-item">
                            @if($request->status)
                                @if($request->status->parent_id != null)
                                    <span style="color: {{ '#'.$request->status->parent->color }}"><b>{{ $request->status->parent->name }}</b> <i class="fa fa-circle"></i></span>
                                @else
                                    <span style="color: {{ '#'.$request->status->color }}"><b>{{ $request->status->name }}</b> <i class="fa fa-circle"></i></span>
                                @endif
                            @else
                                <span style="color: #757575"><b>En attente de validation</b> <i class="fa fa-circle"></i></span>
                            @endif
                            @if($request->updated_at != $request->created_at)
                                <br>Mise a jour le {{ $request->updated_at }}
                            @endif
                        </li>
                        @if($request->user)
                            <li class="list-group-item">Responsable: {{ $request->user->name }}</li>
                        @endif
                    </ul>
                </div>
            </div>

            <file-component
                :app_url="'{{url('/')}}'"
                :user="{{ auth()->user() }}"
                :request="{{ $request }}"
                :update="{{ ($update == '1') ? true : false }}"
            ></file-component>

            @if($update)
                <div class="panel panel-default">
                    <div class="panel-heading">
                        Settings
                    </div>
                    <div class="panel-body">
                        <div class="form-group row">
                            <label class="col-md-4 col-form-label text-md-right">
                                District
                            </label>
                            <div class="col-md-8">
                                <input value="{{ $request->district->name_en }}" class="form-control" disabled>
                            </div>
                        </div>
                        <form method="post" action="{{ route('update_request') }}">
                            @csrf
                            <input type="hidden" name="request_id" value="{{ $request->id }}">
                            <div class="form-group row">
                                <label class="col-md-4 col-form-label text-md-right">
                                    Responsable
                                </label>
                                <div class="col-md-8">
                                    <select class="form-control" name="user">
                                        <option value="">--</option>
                                        @foreach($users as $user)
                                            <option value="{{ $user->id }}" {{ ($request->user_id == $user->id)?'selected':'' }}>{{ $user->name }}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                            <div class="form-group row">
                                <label class="col-md-4 col-form-label text-md-right">
                                    Status
                                </label>
                                <div class="col-md-8">
                                    <select class="form-control" name="status">
                                        @foreach($status as $statu)
                                            <option value="{{ $statu->id }}" {{ ($request->status_id == $statu->id)?'selected':'' }}>{{ $statu->name }}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                            <div class="form-group row">
                                <label class="col-md-4 col-form-label text-md-right">
                                    Request Information
                                </label>
                                <div class="col-md-8">
                                    <input type="checkbox" name="request_information" {{ ($request->request_information==1)?'checked':'' }}>
                                </div>
                            </div>
                            <div class="form-group row">
                                <div class="col-md-8 offset-md-4">
                                    <button class="btn btn-dark">
                                        Update <i class="fa fa-floppy-o" aria-hidden="true"></i>
                                    </button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            @endif

            <div class="panel panel-default">
                <div class="panel-heading">
                    Gov Request
                </div>
                <div class="panel-body">
                    <form method="POST" action="{{ route('store_gov_request') }}" enctype="multipart/form-data">
                        @csrf
                        <input type="hidden" name="requests" value="{{ $request->id }}">
                        <div class="form-group row">
                            <label for="request_type" class="col-md-4 col-form-label text-md-right">
                                Type *
                            </label>
                            <div class="col-md-8" id="request_type">
                                <select name="request_type" required class="form-control">
                                    <option value="">Select Type</option>
                                    <option value="Question">Question</option>
                                    <option value="Answer">Answer</option>
                                    <option value="Other">Other</option>
                                </select>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="title" class="col-md-4 col-form-label text-md-right">
                                Title *
                            </label>
                            <div class="col-md-8">
                                <input id="title" type="text" class="form-control" name="title" required>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="description" class="col-md-4 col-form-label text-md-right">
                                Description *
                            </label>
                            <div class="col-md-8">
                                <textarea id="description" name="description" class="form-control" required></textarea>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label class="col-md-4 col-form-label text-md-right">
                                District *
                            </label>
                            <div class="col-md-8">
                                <select class="form-control" name="district" required>
                                    <option value="">--</option>
                                    @foreach($districts as $district)
                                        <option value="{{ $district->id }}">{{ $district->name_en }}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label class="col-md-4 col-form-label text-md-right">
                                Category *
                            </label>
                            <div class="col-md-8">
                                <select class="form-control" name="category" required>
                                    <option value="">--</option>
                                    @foreach($categories as $category)
                                        <option value="{{ $category->id }}">{{ $category->name_en }}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label class="col-md-4 col-form-label text-md-right">
                                Ministry *
                            </label>
                            <div class="col-md-8">
                                <select class="form-control" required name="ministry">
                                    <option value="">--</option>
                                    @foreach($ministries as $ministry)
                                        <option value="{{ $ministry->id }}">{{ $ministry->name }}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="file" class="col-md-4 col-form-label text-md-right">
                                File
                            </label>
                            <div class="col-md-8">
                                <input id="file" type="file" name="file" required>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="file" class="col-md-4 col-form-label text-md-right">
                                Created on
                            </label>
                            <div class="col-md-8">
                                <div class="input-group date">
                                    <div class="input-group-addon">
                                        <i class="fa fa-calendar"></i>
                                    </div>
                                    <input type="text" class="form-control datepicker pull-right" name="created_on">
                                </div>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="file" class="col-md-4 col-form-label text-md-right">
                                Sent on
                            </label>
                            <div class="col-md-8">
                                <div class="input-group date">
                                    <div class="input-group-addon">
                                        <i class="fa fa-calendar"></i>
                                    </div>
                                    <input type="text" class="form-control datepicker pull-right" name="sent_on">
                                </div>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="file" class="col-md-4 col-form-label text-md-right">
                                Response on
                            </label>
                            <div class="col-md-8">
                                <div class="input-group date">
                                    <div class="input-group-addon">
                                        <i class="fa fa-calendar"></i>
                                    </div>
                                    <input type="text" class="form-control datepicker pull-right" name="response_on">
                                </div>
                            </div>
                        </div>
                        <div class="form-group row">
                            <div class="col-md-8 offset-4">
                                <button type="submit" class="btn btn-primary">
                                    Add
                                </button>
                            </div>
                        </div>
                    </form>
                </div>
                @if($request->gov_reqs)
                    <div class="panel-footer">
                        <ul class="list-group list-group-flush">
                            @foreach($request->gov_reqs as $gov_req)
                                <li class="list-group-item"><a href="{{ route('show_gov_request', [$gov_req->id]) }}">{{ $gov_req->title }}</a></li>
                            @endforeach
                        </ul>
                    </div>
                @endif
            </div>
        </div>
    </div>

@endsection

@section('after_script')
    <!-- bootstrap datepicker -->
    <script src="{{ asset('bower_components/bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js') }}" defer></script>
    <script>
        $(function () {
            //Date picker
            $('.datepicker').datepicker({
                autoclose: true,
                dateFormat: 'yyyy-mm-dd',
                format: 'yyyy-mm-dd'
            });
        });
    </script>
@endsection

