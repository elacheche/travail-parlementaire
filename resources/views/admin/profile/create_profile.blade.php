@extends('admin.layouts.app')

@section('section_title')
    Add New Profile
@endsection

@section('content')

    <div class="row">
        <div class="col-md-6">
            @include('errors.errors')
            <form method="POST" action="{{ route('store_new_profile') }}" enctype="multipart/form-data">
                @csrf
                <div class="form-group row">
                    <label for="first_name" class="col-md-5 col-form-label text-md-right">
                        {{ __('District') }} *
                    </label>
                    <div class="col-md-6">
                        <select class="form-control{{ $errors->has('district') ? ' is-invalid' : '' }}"
                                name="district"
                                required
                        >
                            <option>- Sélection votre Circonscription -</option>
                            @foreach($districts as $district)
                                <option value="{{ $district->id }}">{{ $district->name_en }}</option>
                            @endforeach
                        </select>
                    </div>
                </div>
                <div class="form-group row">
                    <label for="avatar" class="col-md-5 col-form-label text-md-right">
                        {{ __('Avatar') }}
                    </label>
                    <div class="col-md-6">
                        <input id="avatar"
                               type="file"
                               name="avatar"
                               value="{{ old('avatar') }}">
                    </div>
                </div>
                <div class="form-group row">
                    <label for="name_ar" class="col-md-5 col-form-label text-md-right">
                        {{ __('Profile Name AR') }} *
                    </label>
                    <div class="col-md-6">
                        <input id="name_ar"
                               type="text"
                               class="form-control{{ $errors->has('name_ar') ? ' is-invalid' : '' }}"
                               name="name_ar"
                               value="{{ old('name_ar') }}" required>
                    </div>
                </div>
                <div class="form-group row">
                    <label for="name_fr" class="col-md-5 col-form-label text-md-right">
                        {{ __('Profile Name FR') }} *
                    </label>
                    <div class="col-md-6">
                        <input id="name_fr"
                               type="text"
                               class="form-control{{ $errors->has('name_fr') ? ' is-invalid' : '' }}"
                               name="name_fr"
                               value="{{ old('name_fr') }}" required>
                    </div>
                </div>
                <div class="form-group row">
                    <label for="name_en" class="col-md-5 col-form-label text-md-right">
                        {{ __('Profile Name EN') }} *
                    </label>
                    <div class="col-md-6">
                        <input id="name_en"
                               type="text"
                               class="form-control{{ $errors->has('name_en') ? ' is-invalid' : '' }}"
                               name="name_en"
                               value="{{ old('name_en') }}" required>
                    </div>
                </div>
                <div class="form-group row">
                    <label for="email"
                           class="col-md-5 col-form-label text-md-right">{{ __('E-Mail Address *') }}</label>
                    <div class="col-md-6">
                        <input id="email" type="email"
                               class="form-control{{ $errors->has('email') ? ' is-invalid' : '' }}" name="email"
                               value="{{ old('email') }}" required>
                    </div>
                </div>
                <div class="form-group row">
                    <label for="phone_number"
                           class="col-md-5 col-form-label text-md-right">{{ __('Phone number *') }}</label>
                    <div class="col-md-6">
                        <input id="phone_number" type="text"
                               class="form-control{{ $errors->has('phone_number') ? ' is-invalid' : '' }}"
                               name="phone_number" value="{{ old('phone_number') }}" required>
                    </div>
                </div>
                <div class="form-group row">
                    <div class="col-md-6 offset-5">
                        <button type="submit" class="btn btn-primary">
                            {{ __('Add New Profile') }}
                        </button>
                    </div>
                </div>
            </form>
        </div>
    </div>

@endsection
