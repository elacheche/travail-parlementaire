@extends('layouts.app')

@section('content')
    <div class="container" style="margin-top: 100px; margin-bottom: 50px;">
        <div class="row justify-content-center">
            <div class="col-md-8">
                <div class="card">
                    <div class="card-body">
                        <h1>@lang('layout.confidentialityTitle')</h1>
                        {!! __('layout.confidentiality') !!}
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
