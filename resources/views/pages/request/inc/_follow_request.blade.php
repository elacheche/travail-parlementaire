<div class="card shadow">
    <div class="card-body">
        <h2 style="font-family: 'poppins', sans-serif">@lang('layout.follow_request')</h2>
        <hr>
        @include('errors.errors')
        <form method="POST" action="{{ route('show_request', [app()->getLocale()]) }}">
            @csrf
            <div class="form-group row">
                <label for="cin" class="col-sm-4 col-form-label text-md-right">@lang('layout.id')</label>
                <div class="col-md-7">
                    <input id="cin" type="text" class="form-control form-control-lg" name="cin" value="{{ old('cin')?old('cin'):session('cin')?session('cin'):'' }}" required>
                </div>
            </div>
            <div class="form-group row">
                <label for="password" class="col-md-4 col-form-label text-md-right">@lang('layout.password')</label>
                <div class="col-md-7">
                    <input id="password" type="text" class="form-control form-control-lg" name="password" value="{{ session('password') }}" required>
                </div>
            </div>
            <div class="form-group row mb-0">
                <div class="col-md-8 offset-md-4">
                    <button type="submit" class="btn btn-lg btn-primary">
                        @lang('layout.follow')
                    </button>
                </div>
            </div>
        </form>
    </div>
</div>
