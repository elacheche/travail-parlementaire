<?php

namespace App\Http\Controllers\Admin;

use App\Models\Role;
use App\Models\UserType;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class UserTypeController extends Controller
{

    public function index()
    {
        return view('admin.super.settings.user_type.user_types');
    }

    public function show($role_id)
    {
        $role = Role::where('id', $role_id)->firstOrFail();
        $permissions = json_decode($role->permissions);
        return view('admin.super.settings.user_type.view_role', compact('role', 'permissions'));
    }

    public function create()
    {
        return view('admin.super.settings.user_type.create_new_user_type');
    }

    public function store(Request $request)
    {
        $this->validate(
            $request,
            [
                'name' => 'required',
                'super_admin' => 'required',
            ]
        );
        Role::create(
            [
                'name' => $request->name,
                'super_admin' => $request->super_admin,
                'permissions' => ((int)$request->super_admin == 0) ? json_encode($request->privileges) : null
            ]
        );
        session()->flash('message', 'A new user type has been created!');
        return redirect()->route('user_types');
    }

    public function edit($user_type_id)
    {
        $role = Role::where('id', $user_type_id)->firstOrFail();
        return view('admin.super.settings.user_type.edit_user_type', compact('role'));
    }

    public function update($user_type_id, Request $request)
    {
        Role::where('id', $user_type_id)->update(
            [
                'name' => $request->name,
                'super_admin' => $request->super_admin,
                'permissions' => ((int)$request->super_admin == 0) ? json_encode($request->privileges) : null
            ]
        );

        session()->flash('message', 'A new user type has been created!');
        return redirect()->route('user_types');
    }

    public function remove($user_type_id)
    {
        $role = Role::where('id', $user_type_id)->with('users')->firstOrFail();
        if ($role->users == null) {
            $role->delete();
            session()->flash('message', 'A new user type has been removed!');
        } else {
            session()->flash('message', 'A new user type cant been removed!');
        }

        return redirect()->route('user_types');
    }

}
