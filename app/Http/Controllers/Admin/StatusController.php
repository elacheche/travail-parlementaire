<?php

namespace App\Http\Controllers\Admin;

use App\Models\Status;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class StatusController extends Controller
{

    public function index()
    {
        return view('admin.super.settings.status.status');
    }

    public function create()
    {
        $parent_status = Status::Active()->where('parent_status', null)->get();
        return view('admin.super.settings.status.create_new_status', compact('parent_status'));
    }

    public function store(Request $request)
    {
        $this->validate(
            $request,
            [
                'name' => 'required',
                'color' => 'required',
                'active' => 'required'
            ]
        );

        Status::create(
            [
                'name' => $request->name,
                'color' => str_replace('#', '', $request->color),
                'active' => $request->active,
                'parent_status' => $request->parent_status ? $request->parent_status : null
            ]
        );
        session()->flash('message', 'A new status has been created!');
        return redirect()->route('status');
    }

    public function remove($status_id)
    {
        $status = Status::where('id', $status_id)->first();
        $status->delete();
        session()->flash('message', 'A status has been removed!');
        return redirect()->route('status');
    }
}
