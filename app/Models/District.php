<?php

namespace App\Models;

use App\User;
use Illuminate\Database\Eloquent\Model;

class District extends Model
{

    protected $fillable = ['name_fr', 'name_ar', 'name_en', 'active'];

    public function users()
    {
        return $this->hasMany(User::class, 'district_id', 'id');
    }

    public function profiles()
    {
        return $this->hasMany(Profile::class, 'district_id', 'id');
    }

    public function scopeActive($query)
    {
        return $query->where('active', 1);
    }

}
