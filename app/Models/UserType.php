<?php

namespace App\Models;

use App\User;
use Illuminate\Database\Eloquent\Model;

class UserType extends Model
{
    protected $fillable = ['name', 'active'];

    public function users()
    {
        return $this->hasMany(User::class, 'user_type', 'id');
    }

    public function scopeActive($query)
    {
        return $query->where('active', 1);
    }
}
