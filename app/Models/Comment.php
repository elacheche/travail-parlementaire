<?php

namespace App\Models;

use App\User;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Crypt;

class Comment extends Model
{
    protected $fillable = ['request_id', 'user_id', 'applicant_id', 'hidden', 'body'];

    public function request()
    {
        return $this->belongsTo(Request::class, 'request_id', 'id');
    }

    public function applicant()
    {
        return $this->belongsTo(Applicant::class, 'applicant_id', 'id');
    }

    public function user()
    {
        return $this->belongsTo(User::class, 'user_id', 'id');
    }

    public function setBodyAttribute($value)
    {
        $this->attributes['body'] = Crypt::encryptString($value);
    }

    public function getBodyAttribute($value)
    {
        return Crypt::decryptString($value);
    }

}
